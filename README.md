In this R file we created multiple classifiers with glioblastoma data (gene influences) : 

- Pamr

- SVM (Support vector machine)

- Random Forest 

We also used those classifiers to perform predictions, and we compared them (accuracy, kappa, sensitivity, etc).

Author : Marion Estoup

Mail : marion_110@hotmail.fr

Date : August 2023
